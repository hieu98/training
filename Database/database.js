class Database {
    constructor() {
        this.productTable = [];
        this.categoryTable = [];
        this.accesssoryTable = []
    }
    insertTable(name, row) {
        if (name === 'product') {
            this.productTable.push(row)
            return this.productTable
        }

        else if (name === 'category') {
            this.categoryTable.push(row)
            return this.categoryTable
        }

        else if (name === 'accesssory') {
            this.accesssoryTable
            this.accesssoryTable.push(row)
            return this.accesssoryTable
        }

    }

    selectTable(name, where) {
        for (let i = 0; i < this.productTable.length; i++) {
            if (name === 'product' && this.productTable[i].id == where) {
                return this.productTable[i]
            }
        }
        for (let i = 0; i < this.categoryTable.length; i++) {
            if (name === 'category' && this.categoryTable[i].id == where) {
                return this.categoryTable[i]
            }
        }
        for (let i = 0; i < this.accesssoryTable.length; i++) {
            if (name === 'accesssory' && this.accesssoryTable[i].id == where) {
                return this.accesssoryTable[i]
            }
        }
    }
    updateTable(name, row) {
        if (name === 'product') {
            this.productTable.splice(1, 1, row);
            return this.productTable
        }
        else if (name === 'category') {
            this.categoryTable.splice(1, 1, row);
            return this.categoryTable
        }
        else if (name === 'accessssory') {
            this.accessoryTable.splice(1, 1, row);
            return this.accesssoryTable
        }
        else {
            return false
        }
    }
    deleteTable(name, row) {
        if (name === 'product') {
            this.productTable.splice(2, row)
            return this.productTable
        }
        else if (name === 'category') {
            this.categoryTable.splice(2, row)
            return this.categoryTable
        }
        else if (name === 'accesssory') {
            this.accesssoryTable.splice(2, row)
            return this.accesssoryTable
        }
        else {
            return false
        }
    }
    truncateTable(name) {
        if (name === 'product') {
            this.productTable.splice(3);
            return this.productTable
        }
        else if (name === 'category') {
            this.categoryTable.splice(3);
            return this.categoryTable
        }
        else if (name === 'accesssory') {
            this.accesssoryTable.splice(3);
            return this.accesssoryTable
        }
        else {
            return false
        }
    }
    updateTableDemo(name, intergerId, row) {
        if (name === 'product') {
            for (let i = 0; i < this.productTable.length; i++) {
                if (intergerId === this.productTable[i].id) {
                    this.productTable.splice(i, 1, row)
                    return this.productTable
                }
            }
        }

        if (name === 'category') {
            for (let i = 0; i < this.categoryTable.length; i++) {
                if (intergerId === this.categoryTable[i].id) {
                    this.categoryTable.splice(i, 1, row)
                    return this.categoryTable
                }
            }
        }

        for (let i = 0; i < this.accesssoryTable.length; i++) {
            if (intergerId === this.accesssoryTable[i].id) {
                this.accesssoryTable.splice(i, 1, row)
                return this.accesssoryTable
            }
        }
    }
}
var object = new Database()

object.insertTable('product', { id: 1238, name: 'product1', categoryId: 1237, quality: 40 })
object.insertTable('product', { id: 1248, name: 'product2', categoryId: 1233, quality: 60 })
object.insertTable('product', { id: 1258, name: 'product3', categoryId: 1235, quality: 20 })
object.insertTable('product', { id: 1268, name: 'product4', categoryId: 1231, quality: 70 })

object.insertTable('category', { id: 1278, name: 'category1', categoryId: 1237, quality: 40 })
object.insertTable('category', { id: 1298, name: 'category2', categoryId: 1233, quality: 60 })
object.insertTable('category', { id: 1208, name: 'category3', categoryId: 1235, quality: 20 })
object.insertTable('category', { id: 1218, name: 'category4', categoryId: 1231, quality: 70 })

object.insertTable('accesssory', { id: 1228, name: 'accesssory1', categoryId: 1237, quality: 40 })
object.insertTable('accesssory', { id: 1219, name: 'accesssory2', categoryId: 1233, quality: 60 })
object.insertTable('accesssory', { id: 1229, name: 'accesssory3', categoryId: 1235, quality: 20 })
object.insertTable('accesssory', { id: 1239, name: 'accesssory4', categoryId: 1231, quality: 70 })

console.log(object);

console.log(object.selectTable('category', 1278));

console.log(object.updateTable('category', {}));

console.log(object.deleteTable('accesssory', 2));

console.log(object.truncateTable('category'));

console.log(object.updateTableDemo('category', 1208, {}))