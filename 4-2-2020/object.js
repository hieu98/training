var Student = {
    data : [],
    viewStudent : function(){
        var listStudent = this.data;
        for(var i = 0; i < listStudent.length; i++){
            document.write("<div>" + listStudent[i].id + "|" + listStudent[i].name + "|" + listStudent[i].email + "</div>");
        }
    },
    addStudent : function(id, name, email){
        var item = {
            id : id,
            name : name,
            email : email
        };
        this.data.push(item);
    },
    removeStudent : function(id){
        for(var i = 0; i < this.data.length; i++){
            if (this.data[i].id === id) {
                this.data.splice(i, 1); 
            }
        }
    },
    editStudent : function(id, name, email){
        for(var i = 0; i < this.data.length; i++){
            if (this.data[i].id === id) { 
                this.data[i].name = name;
                this.data[i].email = email;
            }
        }
    }
};
document.write('<h2>Danh sách sinh viên lúc đầu</h2>');
Student.viewStudent();

document.write('<h2>Danh sách sinh viên thêm ba người</h2>');
Student.addStudent("1", 'Nguyễn Văn a', "abc@gmail.com");
Student.addStudent("2", 'Nguyễn Văn b', "def@gmail.com");
Student.addStudent("3", 'Nguyễn Văn c', "efg@gmail.com");
Student.viewStudent();

document.write('<h2>Danh sách sinh viên xóa một người</h2>');
Student.removeStudent('3');
Student.viewStudent();

document.write('<h2>Danh sách sinh viên sửa thông tin</h2>');
Student.editStudent('2', "TênMới", "...@gmail.com");
Student.viewStudent();


document.write("<br>");
Number.prototype.plus = function(value){
    return this.valueOf() + parseInt(value);
};
var age = Number(22);
document.write(age.plus(22) +"<br>");
var year = 2020;
document.write(year.plus(22));

document.write("<br>");
function Person(){
}
Person.prototype.gender = "";
Person.prototype.showGender = function(){
  document.write(this.gender);  
};
var hieu = new Person();
hieu.gender = "Men";
hieu.showGender();