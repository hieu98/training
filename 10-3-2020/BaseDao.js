class BaseDao {
    constructor() {
        this.baseDao = [];
    }
    insertTable(name, row) {
        if (name === 'product') {
            this.baseDao.push(row)
            return this.baseDao
        }
        else if (name === 'category') {
            this.baseDao.push(row)
            return this.baseDao
        }
        else if (name === 'accesssory') {
            this.baseDao.push(row)
            return this.baseDao
        }
    }
    updateTable(name, row) {
        if (name === 'product') {
            this.baseDao.splice(1, 1, row)
            return this.baseDao
        }
        else if (name === 'category') {
            this.baseDao.splice(1, 1, row)
            return this.baseDao
        }
        else if (name === 'accesssory') {
            this.baseDao.splice(1, 1, row)
            return this.baseDao
        }
    }

    deleteTable(name, row) {
        if (name === 'product') {
            this.baseDao.splice(row, 1)
            return this.baseDao
        }
        else if (name === 'category') {
            this.baseDao.splice(row, 1)
            return this.baseDao
        }
        else if (name === 'accesssory') {
            this.baseDao.splice(row, 1)
            return this.baseDao
        }
    }
    findAll() {
        return this.baseDao
    }
    findById(id) {
        for (let i = 0; i < this.baseDao.length; i++) {
            if (id === this.baseDao[i].id) {
                return this.baseDao[i]
            }
        }
    }

    findByName(name) {
        for (let i = 0; i < this.baseDao.length; i++) {
            if (name === this.baseDao[i].name) {
                return this.baseDao[i]
            }
        }
    }

}
