class CategoryDao extends BaseDao {
    constructor() {
        super()
    } 
}

var object = new CategoryDao()

object.insertTable('category',{id:1288,name: 'category1', categoryId: 1237})
object.insertTable('category',{id:1298,name: 'category2',categoryId: 1233})
object.insertTable('category',{id:1209, name:'category3',categoryId: 1235})
object.insertTable('category',{id:1219, name:'category4',categoryId: 1231})

console.log(object);

console.log(object.updateTable('category',{}));

console.log(object.deleteTable('category',2));

console.log(object.findAll());

console.log(object.findById(1288));