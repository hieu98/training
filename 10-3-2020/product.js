class Product extends BaseRow{
    constructor(id, name) {
        super(id,name)
        this.id = id;
        this.name = name;
    }
}

var objectProduct = new Product();
objectProduct.id = 1234
objectProduct.name = 'Productabc'
console.log(objectProduct.id);
console.log(objectProduct.name);
