class AccesssoryDao extends BaseDao {
    constructor() {
        super()
    }
}

var object = new AccesssoryDao()
object.insertTable('accesssory',{ id: 1228, name: 'accesssory1', categoryId: 1237 })
object.insertTable('accesssory',{ id: 1278, name: 'accesssory2', categoryId: 1233 })
object.insertTable('accesssory',{ id: 1208, name: 'accesssory3', categoryId: 1235 })
object.insertTable('accesssory',{ id: 1218, name: 'accesssory4', categoryId: 1231 })

console.log(object);

console.log(object.updateTable('accesssory',{}));

console.log(object.deleteTable('accesssory',2));

console.log(object.findAll());

console.log(object.findById(1218));

console.log(object.findByName('accesssory1'));