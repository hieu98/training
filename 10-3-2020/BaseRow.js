class BaseRow {
    constructor(id, name) {
        this.id = id;
        this.name = name
    }
    get id() {
        return this.Id
    }
    set id(id) {
            this.Id = id;
    }
    get name() {
        return this.Name
    }
    set name(name) {
            this.Name = name;
    }
}

//     insertTable(name, row) {
//         if (name === 'baserow') {
//             this.baseRow.push(row)
//             return this.baseRow
//         }
//     }

//     selectTable(name, where) {
//         if (name === 'baserow') {
//             for (let i = 0; i < this.baseRow.length; i++) {
//                 if (this.baseRow[i].id === where) {
//                     return this.baseRow[i]
//                 }
//             }
//         }
//     }

//     updateTable(name, row) {
//         if (name === 'baserow') {
//             this.baseRow.splice(1, 1, row)
//             return this.baseRow
//         }
//     }



//     deleteTable(name, row) {
//         if (name === 'baserow') {
//             this.baseRow.splice(row, 1)
//             return this.baseRow
//         }
//     }

//     truncateTable(name) {
//         if (name === 'baserow') {
//             this.baseRow.splice(2)
//             return this.baseRow
//         }
//     }
// }

// var object = new BaseRow()

// object.insertTable('baserow', { id: 1234, name: 'baserow0', categoryId: 1222 })
// object.insertTable('baserow', { id: 1235, name: 'baserow1', categoryId: 1333 })
// object.insertTable('baserow', { id: 1236, name: 'baserow2', categoryId: 1444 })
// object.insertTable('baserow', { id: 1237, name: 'baserow3', categoryId: 1555 })

// console.log(object.baseRow)

// console.log(object.selectTable('baserow', 1235));

// console.log(object.updateTable('baserow', {}));

// console.log(object.deleteTable('baserow', 3));

// console.log(object.truncateTable('baserow'));

// class ProductEntity extends BaseRow {
//     constructor() {
//         super();
//     }
// }

// var objectProduct = new ProductEntity()

// objectProduct.insertTable('baserow', { id: 1238, name: 'product1', categoryId: 1237 })
// objectProduct.insertTable('baserow', { id: 1248, name: 'product2', categoryId: 1233 })
// objectProduct.insertTable('baserow', { id: 1258, name: 'product3', categoryId: 1235 })
// objectProduct.insertTable('baserow', { id: 1268, name: 'product4', categoryId: 1231 })

// console.log(objectProduct.baseRow);

// console.log(objectProduct.selectTable('baserow', 1238));

// console.log(objectProduct.updateTable('baserow', {}));

// console.log(objectProduct.deleteTable('baserow', 3));

// console.log(objectProduct.truncateTable('baserow'));

// class CategoryEntity extends BaseRow {
//     constructor() {
//         super();
//     }
// }

// var objectCategory = new CategoryEntity()

// objectCategory.insertTable('baserow', { id: 1288, name: 'category1', categoryId: 1237 })
// objectCategory.insertTable('baserow', { id: 1298, name: 'category2', categoryId: 1233 })
// objectCategory.insertTable('baserow', { id: 1209, name: 'category3', categoryId: 1235 })
// objectCategory.insertTable('baserow', { id: 1219, name: 'category4', categoryId: 1231 })

// console.log(objectCategory.baseRow);

// console.log(objectCategory.selectTable('baserow', 1288));

// console.log(objectCategory.updateTable('baserow', {}));

// console.log(objectCategory.deleteTable('baserow', 3));

// console.log(objectCategory.truncateTable('baserow'));

// class AccesssoryEntity extends BaseRow {
//     constructor() {
//         super();
//     }
// }

// var objectAccesssory = new AccesssoryEntity()

// objectAccesssory.insertTable('baserow', { id: 1228, name: 'accesssory1', categoryId: 1237 })
// objectAccesssory.insertTable('baserow', { id: 1278, name: 'accesssory2', categoryId: 1233 })
// objectAccesssory.insertTable('baserow', { id: 1208, name: 'accesssory3', categoryId: 1235 })
// objectAccesssory.insertTable('baserow', { id: 1218, name: 'accesssory4', categoryId: 1231 })

// console.log(objectAccesssory.baseRow);

// console.log(objectAccesssory.selectTable('baserow', 1228));

// console.log(objectAccesssory.updateTable('baserow', {}));

// console.log(objectAccesssory.deleteTable('baserow', 3));

// console.log(objectAccesssory.truncateTable('baserow'));