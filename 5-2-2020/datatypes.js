var string = "xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.indexOf ("hello"))

document.write ("<br>")

var string = "xin chào Việt Nam hello xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.lastIndexOf ("hello"))

document.write ("<br>")

var string = "xin chào Việt Nam hello xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.search ("hello"))

document.write ("<br>")

var string = "xin chào Việt Nam hello xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.slice (18,23))

document.write ("<br>")

var string = "xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.slice (-5,23))

document.write ("<br>")

var string = "xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.slice (4))
document.write ("<br>")

var string = "xin chào Việt Nam hello"
document.write ("vị trí xuất  hiện chuỗi là: " + string.substring (18,23))

document.write ("<br>")

var string = "xin chào Việt Nam hello";
document.write(string.replace("hello", "wellcome"));

document.write ("<br>")

var string = "xin chào Việt Nam hello";
document.write(string.toUpperCase() + "<br>");
document.write(string.toLowerCase());

document.write ("<br>")

var string = "xin chào " + "việt Nam " + " hello";
document.write(string);

document.write ("<br>")

var string = "xin chào việt nam";
document.write(string.charAt(2) + "<br/>");
document.write(string.charCodeAt(1))

document.write ("<br>")

string = "xin chào việt nam";
document.write(string.split(" ").length);

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
document.write(mang.valueOf());

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
document.write(mang.valueOf());
document.write('<br>');
mang.push("Wellcome");
document.write(mang.valueOf());

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
document.write(mang.valueOf());
document.write('<br>');
mang.pop();
document.write(mang.valueOf());

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
document.write(mang.valueOf());
document.write('<br>');
mang.shift();
document.write(mang.valueOf());

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
document.write(mang.valueOf());
document.write('<br>');
mang.unshift("Wellcome");
document.write(mang.valueOf());

document.write ("<br>")

var mang1 = ["xin","chào", "Việt","Nam"]
mang1.splice(0 , 2, "bạn đã đến" );
document.write(mang1.valueOf());

document.write ("<br>")

var mang = ["d","b", "c","a"]
mang.sort();
document.write(mang.valueOf());

document.write ("<br>")

var mang = ["a","b", "c","d"]
mang.reverse();
document.write(mang.valueOf())

document.write ("<br>")

var mang1 = ["a", "b", "c"];
var mang2 = ["d", "e"];
var mang_con = mang1.concat(mang2);
document.write(mang_con.valueOf());

document.write ("<br>")

var mang = ["xin","chào", "Việt","Nam"]
var mang_moi = mang.slice (2,4)
document.write(mang_moi.valueOf())

document.write ("<br>")

var x = 5.789;
document.write(x.toFixed(0) + "<br>");
document.write(x.toFixed(2) + "<br>");
document.write(x.toFixed(4) + "<br>");
document.write(x.toFixed(6));

document.write ("<br>")

var x = 5.789;
document.write(x.toPrecision() + "<br>");
document.write(x.toPrecision(2) + "<br>");
document.write(x.toPrecision(4) + "<br>");
document.write(x.toPrecision(6));

document.write ("<br>")

var x = 123 + 17;
document.write(x.valueOf() + "<br>");
document.write((3 + 3).valueOf());

document.write ("<br>")

var date0bj = new Date();
document.write(date0bj);

document.write ("<br>")

var date0bj = new Date(2020,1,20);
document.write(date0bj);

document.write ("<br>")

var d = new Date();
document.write(d.getDate()      + "<br>");
document.write(d.getDay()       + "<br>");
document.write(d.getFullYear()  + "<br>");
document.write(d.getYear()      + "<br>");
document.write(d.getHours()     + "<br>");
document.write(d.getMilliseconds() + "<br>");
document.write(d.getMinutes()   + "<br>");
document.write(d.getMonth()     + "<br>");
document.write(d.getSeconds()   + "<br>");
document.write(d.getTime());

document.write ("<br>")

d.setDate(20);
d.setFullYear(2020);
d.setHours(14);
d.setMilliseconds(2);
d.setMinutes(37);
d.setMonth(1);
d.setSeconds(5);

document.write(d.getDate()      + "<br>");
document.write(d.getDay()       + "<br>");
document.write(d.getFullYear()  + "<br>");
document.write(d.getYear()      + "<br>");
document.write(d.getHours()     + "<br>");
document.write(d.getMilliseconds() + "<br>");
document.write(d.getMinutes()   + "<br>");
document.write(d.getMonth()     + "<br>");
document.write(d.getSeconds()   + "<br>");
document.write(d.getTime());

document.write ("<br>")

function startTime(){
var today = new Date();
var h = today.getHours();
var m = today.getMinutes();
var s = today.getSeconds();
m = checkTime(m);
s = checkTime(s);
document.getElementById('timer').innerHTML = h + ":" + m + ":" + s;
var t = setTimeout(function() {
    startTime();
     }, 500);
        }
function checkTime(i) 
{
    if (i < 10) {
      i = "0" + i;
             }
    return i;
            }