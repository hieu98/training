<?php
include_once 'Accessory.php';

class AccessoryDemo extends Accessory
{
   public function  createAccessoryTest()
   {
    $this->setId(123);
    $this->getId();

    $this->setName('product1');
    $this->getName();

    $this->setCategoryId(1245);
    $this->getCategoryId();
    
    $this->setQuality(100);
    $this->getQuality();
   }

   public function printTableTest()
   {
    $this->createAccessoryTest();
   }
}

$accessory = new AccessoryDemo();
$accessory->printTableTest();
print_r($accessory);
