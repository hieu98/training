<?php

class BaseRow
{
   public $id;
   public $name ;
   public $categoryId ;
   public $quality ;

  public function getId()
  {
    return  $this->id;
  }

  public function getName()
  {
    return  $this->name;
  }

  public function getCategoryId()
  {
    return  $this->categoryId;
  }

  public function getQuality()
  {
    return  $this->quality;
  }

}