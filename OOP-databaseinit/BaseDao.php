<?php
include_once 'Database.php';
class BaseDao extends Database
{
    public function insert ($row,$name)
    {
        $this->insertTable($row, $name);
    }

    public function update($name,$row,$id)
    {
    $this->updateTable( $name,$row,$id);
    }

    public function  delete($name,$row)
    {
      $this->deleteTable($name,$row);
    }

    public function findAll($where) 
    {
        
      return $this-> $where;
    }

    public function findById($id,$where)
    {

      foreach ($this->$where as $value)
        {
          if ($value->id  === $id) {
              return $value;
          }
        }
    }
  public function findByName($name,$where)
  {
    foreach($this->$where as $value)
    {
      if($value->name === $name){
        return $value; 
      }
    }
  }
}