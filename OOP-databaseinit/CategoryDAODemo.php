<?php
include_once 'CategoryDAO.php';
class CategoryDaoDemo extends CategoryDao
{
    public function insertTest()
    {
      $this->insert( ['id' => 1, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12],'category');
      $this->insert( ['id' => 2, 'name' => 'Product2', 'categoryId' => 123 , 'quality' =>12],'category');
    }
    public function updateTest()
    {
     $this->update('category',['id' => 19, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12], 1);
    }
    public function deleteTest()
    {
        $this->delete('category',0);
    }
    public function findAllTest()
    {
        $this->findAll('categoryTable');
    }
    public function findByIdTest()
    {
      return  $this->findById(1,'categoryTable');
    }
    public function findByNameTest()
    {
        $this->findByName('Product2','categoryTable');
    }
}