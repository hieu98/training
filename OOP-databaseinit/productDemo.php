<?php
include_once 'product.php';

class ProductDemo extends Product
{
   public function  createProductTest()
   {
    $this->setId(123);
    $this->getId();

    $this->setName('product1');
    $this->getName();

    $this->setCategoryId(1245);
    $this->getCategoryId();
    
    $this->setQuality(100);
    $this->getQuality();
   }

   public function printTableTest()
   {
    $this->createProductTest();
   }
}

$product = new ProductDemo();
$product->printTableTest();
print_r($product);
