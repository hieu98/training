<?php

class Database
{
    public $productTable = [];
    public $categoryTable = [];
    public $accessoryTable = [];

    public function insertTable( $row, $name)
    {
        switch ($name) {
            case 'product':
                array_push($this->productTable,(object) $row);
                break;
            case 'category':
                array_push($this->categoryTable,(object) $row);
                break;
            case 'accessory':
                array_push($this->accessoryTable,(object) $row);
                break;
        }
    }

    public function selectTable($name, $where)
    {
        $test =(object)[];
        switch ($name) {
            case 'product':
                foreach ($this->productTable as  $value) {
                    if ($value->id  === $where) {
                        return ($value);
                    }
                }
                break;
            case 'category':
                foreach ($this->categoryTable as $value) {
                    if ($value->id  === $where) {
                        return ($value);
                    }
                }
                break;
            case 'accessory':
                foreach ($this->accessoryTable as  $value) {
                    if ($value->id  === $where) {
                        return ($value);
                    }
                }
                break;
        }
    }

    public function updateTable($name, $row,$id)
    {
        switch ($name){
            case 'product':
                foreach ($this->productTable as $key =>$value) {
                    if ($value->id === $id) {
                        array_splice($this->productTable,$key, 1,(object) [$row]);
                    }
                }
                break;
            case 'category':
                foreach ($this->categoryTable as $key =>$value) {
                    if ($value->id === $id) {
                        array_splice($this->categoryTable,$key, 1,(object) [$row]);
                    }
                }
                break;
            case 'accessory':
                foreach ($this->accessoryTable as $key => $value) {
                    if ((object) $value->id === $id) {
                        array_splice($this->accessoryTable,$key, 1,(object) [$row]);
                    }
                }
                break;
        }
    }

    public function deleteTable($name, $row)
    {
        switch ($name){
            case 'product':
                unset($this->productTable[$row]);
                break;
            case 'category':
                unset($this->categoryTable[$row]);
                break;
            case 'accessory':
                unset($this->accessoryTable[$row]);
                break;
        }
    }

    public function truncateTable($name)
    {
        switch ($name){
            case 'product':
                  $this->productTable = [];
                break;
            case 'category':
                $this->categoryTable= [];
                break;
            case 'accessory':
                $this->accessoryTable = [];
                break;
        }
    }
}
