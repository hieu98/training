<?php
include_once 'ProductDAO.php';
class ProductDaoDemo extends ProductDao
{
    public function insertTest()
    {
      $this->insert( ['id' => 1, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12],'product');
      $this->insert( ['id' => 2, 'name' => 'Product2', 'categoryId' => 123 , 'quality' =>12],'product');
    }
    public function updateTest()
    {
     $this->update('product',['id' => 19, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12], 1);
    }
    public function deleteTest()
    {
        $this->delete('product',0);
    }
    public function findAllTest()
    {
        $this->findAll('productTable');
    }
    public function findByIdTest()
    {
      return  $this->findById(1,'productTable');
    }
    public function findByNameTest()
    {
        $this->findByName('Product2','productTable');
    }
}