<?php
include_once 'Database.php';
class DatabaseDemo extends Database
{
    public function insertTableTest()
    {
        $this->insertTable((object) ['id'=> 1238, 'name' => 'product1', 'categoryId' => 1237, 'quality' => 40], 'accessory');
    }
    public function  selectTableTest()
    {
        return $this->selectTable('category', 1278);
    }
    public function  updateTableTest()
    {
        $this->updateTable('accessory',(object) ['id'=> 1249, 'name' => 'product3', 'categoryId' => 1239, 'quality' => 70], 1258);
    }
    public function  deleteTableTest()
    {
        $this->deleteTable('accessory', '1');
    }
    public function  truncateTableTest()
    {
        $this->truncateTable('product');
    }

    public function initDatabase()
    {
        $this ->insertTable((object) ['id'=> 1238, 'name' => 'product1', 'categoryId' => 1237, 'quality' => 40], 'product');
        $this ->insertTable((object) ['id'=> 1239, 'name' => 'product2', 'categoryId' => 1238, 'quality' => 50], 'product');
        $this ->insertTable((object) ['id'=> 1248, 'name' => 'product3', 'categoryId' => 1239, 'quality' => 70], 'product');
        $this ->insertTable((object) ['id'=> 1249, 'name' => 'product4', 'categoryId' => 1230, 'quality' => 90], 'product');
        $this ->insertTable((object) ['id'=> 1258, 'name' => 'product5', 'categoryId' => 1231, 'quality' => 40], 'product');
        $this ->insertTable((object) ['id'=> 1259, 'name' => 'product6', 'categoryId' => 1232, 'quality' => 100],'product');
        $this ->insertTable((object) ['id'=> 1268, 'name' => 'product7', 'categoryId' => 1233, 'quality' => 10], 'product');
        $this ->insertTable((object) ['id'=> 1269, 'name' => 'product8', 'categoryId' => 1234, 'quality' => 30], 'product');
        $this ->insertTable((object) ['id'=> 1278, 'name' => 'product9', 'categoryId' => 1235, 'quality' => 50], 'product');
        $this ->insertTable((object) ['id'=> 1279, 'name' => 'product0', 'categoryId' => 1236, 'quality' => 60], 'product');

        $this ->insertTable((object) ['id'=> 1238, 'name' => 'product1', 'categoryId' => 1237, 'quality' => 40], 'category');
        $this ->insertTable((object) ['id'=> 1239, 'name' => 'product2', 'categoryId' => 1238, 'quality' => 50], 'category');
        $this ->insertTable((object) ['id'=> 1248, 'name' => 'product3', 'categoryId' => 1239, 'quality' => 70], 'category');
        $this ->insertTable((object) ['id'=> 1249, 'name' => 'product4', 'categoryId' => 1230, 'quality' => 90], 'category');
        $this ->insertTable((object) ['id'=> 1258, 'name' => 'product5', 'categoryId' => 1231, 'quality' => 40], 'category');
        $this ->insertTable((object) ['id'=> 1259, 'name' => 'product6', 'categoryId' => 1232, 'quality' => 100],'category');
        $this ->insertTable((object) ['id'=> 1268, 'name' => 'product7', 'categoryId' => 1233, 'quality' => 10], 'category');
        $this ->insertTable((object) ['id'=> 1269, 'name' => 'product8', 'categoryId' => 1234, 'quality' => 30], 'category');
        $this ->insertTable((object) ['id'=> 1278, 'name' => 'product9', 'categoryId' => 1235, 'quality' => 50], 'category');
        $this ->insertTable((object) ['id'=> 1279, 'name' => 'product0', 'categoryId' => 1236, 'quality' => 60], 'category');

        $this ->insertTable((object) ['id'=> 1238, 'name' => 'product1', 'categoryId' => 1237, 'quality' => 40], 'accessory');
        $this ->insertTable((object) ['id'=> 1239, 'name' => 'product2', 'categoryId' => 1238, 'quality' => 50], 'accessory');
        $this ->insertTable((object) ['id'=> 1248, 'name' => 'product3', 'categoryId' => 1239, 'quality' => 70], 'accessory');
        $this ->insertTable((object) ['id'=> 1249, 'name' => 'product4', 'categoryId' => 1230, 'quality' => 90], 'accessory');
        $this ->insertTable((object) ['id'=> 1258, 'name' => 'product5', 'categoryId' => 1231, 'quality' => 40], 'accessory');
        $this ->insertTable((object) ['id'=> 1259, 'name' => 'product6', 'categoryId' => 1232, 'quality' => 100],'accessory');
        $this ->insertTable((object) ['id'=> 1268, 'name' => 'product7', 'categoryId' => 1233, 'quality' => 10], 'accessory');
        $this ->insertTable((object) ['id'=> 1269, 'name' => 'product8', 'categoryId' => 1234, 'quality' => 30], 'accessory');
        $this ->insertTable((object) ['id'=> 1278, 'name' => 'product9', 'categoryId' => 1235, 'quality' => 50], 'accessory');
        $this ->insertTable((object) ['id'=> 1279, 'name' => 'product0', 'categoryId' => 1236, 'quality' => 60], 'accessory');
    }

    public function printTableTest()
    {
       $this->initDatabase();
    //    $this->insertTableTest();
    //    $this->selectTableTest();
    //    $this->updateTableTest();    
       $this->deleteTableTest();
    }
}

$data = new DatabaseDemo();
$data->printTableTest();
echo json_encode($data);
die;


