<?php
include_once 'accessoryDAO.php';
class AccessoryDaoDemo extends AccessoryDao
{
    public function insertTest()
    {
      $this->insert( ['id' => 1, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12],'accessory');
      $this->insert( ['id' => 2, 'name' => 'Product2', 'categoryId' => 123 , 'quality' =>12],'accessory');
    }
    public function updateTest()
    {
     $this->update('accessory',['id' => 19, 'name' => 'Product20', 'categoryId' => 123 , 'quality' =>12], 1);
    }
    public function deleteTest()
    {
        $this->delete('accessory',0);
    }
    public function findAllTest()
    {
        $this->findAll('accessoryTable');
    }
    public function findByIdTest()
    {
      return  $this->findById(1,'accessoryTable');
    }
    public function findByNameTest()
    {
        $this->findByName('Product2','accessoryTable');
    }
}
