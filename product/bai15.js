class Product {
    constructor(Object) {
        this.id = Object.id
        this.name = Object.name
        this.categoryId = Object.categoryId
        this.saleDate = Object.saleDate
        this.quality = Object.quality
        this.isDeleted = Object.isDeleted
    }
}
var product = [
    { id: 1231, name: ('producta'), categoryId: 1230, saleDate: new Date(''), quality: 10, isDeleted: true },
    { id: 1232, name: ('productb'), categoryId: 1231, saleDate: new Date(''), quality: 20, isDeleted: false },
    { id: 1233, name: ('productc'), categoryId: 1232, saleDate: new Date(''), quality: 30, isDeleted: false },
    { id: 1234, name: ('productd'), categoryId: 1233, saleDate: new Date(''), quality: 40, isDeleted: true },
    { id: 1235, name: ('producte'), categoryId: 1234, saleDate: new Date(''), quality: 50, isDeleted: true },
    { id: 1236, name: ('productg'), categoryId: 1235, saleDate: new Date(''), quality: 60, isDeleted: false },
    { id: 1237, name: ('productf'), categoryId: 1236, saleDate: new Date(''), quality: 70, isDeleted: true },
    { id: 1238, name: ('producti'), categoryId: 1237, saleDate: new Date(''), quality: 80, isDeleted: true },
    { id: 1239, name: ('productl'), categoryId: 1238, saleDate: new Date(''), quality: 90, isDeleted: false },
    { id: 1230, name: ('productk'), categoryId: 1239, saleDate: new Date(''), quality: 0, isDeleted: true }
]
function isHaveProductCategory(listProduct,categoryId) {
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].categoryId == categoryId) {
            return true
        }
    }
    return false
}
console.log(isHaveProductCategory(product, 1235));