class Product {
    constructor(object) {
        this.id = object.id,
            this.name = object.name,
            this.categoryld = object.categoryld,
            this.saleDate = object.saleDate,
            this.quality = object.quality,
            this.isDelete = object.isDeleted
    }
}
var product = [
    { id: 1231, name: 'hieua', categoryld: 123, saleDate: Date('14/2/2020'), quality: 0, isDeleted: false },
    { id: 1232, name: 'hieub', categoryld: 123, saleDate: Date('14/2/2020'), quality: 0, isDeleted: true },
    { id: 1233, name: 'hieuc', categoryld: 123, saleDate: Date('14/2/2020'), quality: -1, isDeleted: false },
    { id: 1234, name: 'hieud', categoryld: 123, saleDate: Date('14/2/2020'), quality: -2, isDeleted: false },
    { id: 1235, name: 'hieue', categoryld: 123, saleDate: Date('14/2/2020'), quality: 2, isDeleted: false },
    { id: 1236, name: 'hieuf', categoryld: 123, saleDate: Date('14/2/2020'), quality: 3, isDeleted: true },
    { id: 1237, name: 'hieug', categoryld: 123, saleDate: Date('14/2/2020'), quality: 5, isDeleted: false },
    { id: 1238, name: 'hieuh', categoryld: 123, saleDate: Date('14/2/2020'), quality: 100, isDeleted: false },
    { id: 1239, name: 'hieui', categoryld: 123, saleDate: Date('14/2/2020'), quality: 120, isDeleted: true },
    { id: 1230, name: 'hieuk', categoryld: 123, saleDate: Date('14/2/2020'), quality: 150, isDeleted: false },
]
function filterProductByQuality(listProduct) {
    var result = [];
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].quality > 0 && listProduct[i].isDeleted == false) {
            result.push(listProduct[i])
        }
    }
    return result
}
console.log(filterProductByQuality(product));