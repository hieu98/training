class Product {
    constructor(Object) {
        this.id = Object.id,
            this.name = Object.name,
            this.categoryId = Object.categoryId,
            this.saleDate = Object.saleDate,
            this.quality = Object.quality,
            this.isDeleted = Object.isDeleted
    }
}

var product = [
    { id: 1230, name: ('producta'), categoryId: 1230, saleDate: new Date('2020-02-20').getTime(), quality: 10, isDeleted: true },
    { id: 1231, name: ('productb'), categoryId: 1231, saleDate: new Date('2020-02-14').getTime(), quality: 0, isDeleted: false },
    { id: 1232, name: ('productc'), categoryId: 1232, saleDate: new Date('2020-02-28').getTime(), quality: 0, isDeleted: true },
    { id: 1233, name: ('productd'), categoryId: 1233, saleDate: new Date('2020-01-16').getTime(), quality: 90, isDeleted: true },
    { id: 1234, name: ('producte'), categoryId: 1234, saleDate: new Date('2020-02-20').getTime(), quality: 4, isDeleted: true },
    { id: 1235, name: ('productg'), categoryId: 1235, saleDate: new Date('2020-02-14').getTime(), quality: 20, isDeleted: false },
    { id: 1236, name: ('productf'), categoryId: 1236, saleDate: new Date('2020-02-28').getTime(), quality: -6, isDeleted: true },
    { id: 1237, name: ('producti'), categoryId: 1237, saleDate: new Date('2020-02-14').getTime(), quality: 80, isDeleted: false },
    { id: 1238, name: ('productk'), categoryId: 1238, saleDate: new Date('2020-02-20').getTime(), quality: 1, isDeleted: true },
    { id: 1239, name: ('productl'), categoryId: 1239, saleDate: new Date('2020-02-28').getTime(), quality: 0, isDeleted: true }
]

function filterProductMaxQuality(listProduct) {
    var max= listProduct[0].quality;
    for (let i = 1; i < listProduct.length; i++) {
        if (listProduct[i].quality >= max) {
             max=listProduct[i].quality
            }
        }
        return max
}
console.log(filterProductMaxQuality(product));