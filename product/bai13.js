class Product {
    constructor(object) {
        this.id = object.id,
            this.name = object.name,
            this.categoryld = object.categoryld,
            this.saleDate = object.saleDate,
            this.quality = object.quality,
            this.isDelete = object.isDeleted
    }
}
var product = [
    { id: 1231, name: 'hieua', categoryld: 123, saleDate: new Date('2020-02-02').getTime(), quality: 0,   isDeleted: false },
    { id: 1232, name: 'hieub', categoryld: 123, saleDate: new Date('2020-02-20').getTime(), quality: 0,   isDeleted: true },
    { id: 1233, name: 'hieuc', categoryld: 123, saleDate: new Date('2020-02-02').getTime(), quality: -1,  isDeleted: false },
    { id: 1234, name: 'hieud', categoryld: 123, saleDate: new Date('2020-02-02').getTime(), quality: -2,  isDeleted: false },
    { id: 1235, name: 'hieue', categoryld: 123, saleDate: new Date('2020-02-20').getTime(), quality: 2,   isDeleted: false },
    { id: 1236, name: 'hieuf', categoryld: 123, saleDate: new Date('2020-02-20').getTime(), quality: 3,   isDeleted: true },
    { id: 1237, name: 'hieug', categoryld: 123, saleDate: new Date('2020-02-02').getTime(), quality: 5,   isDeleted: false },
    { id: 1238, name: 'hieuh', categoryld: 123, saleDate: new Date('2020-02-30').getTime(), quality: 100, isDeleted: false },
    { id: 1239, name: 'hieui', categoryld: 123, saleDate: new Date('2020-02-02').getTime(), quality: 120, isDeleted: true },
    { id: 1230, name: 'hieuk', categoryld: 123, saleDate: new Date('2020-02-30').getTime(), quality: 150, isDeleted: false },
]


function filterProductBySaleDate(listProduct) {
    var result = [];
    var date = new Date().getTime();
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].saleDate > date && listProduct[i].isDeleted == false) {
            result.push(listProduct[i])
        }
    }
    return result
}
console.log(filterProductBySaleDate(product));

