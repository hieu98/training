class CategoryDao {
    constructor() {
        this.categoryDao = []
    }
    insertTable(row) {
        this.categoryDao.push(row)
        return this.categoryDao
    }

    updateTable(row) {
        this.categoryDao.splice(1, 1, row)
        return this.categoryDao
    }

    deleteTable(row) {
        this.categoryDao.splice(row, 1)
        return this.categoryDao
    }

    findAll() {
        return this.categoryDao
    }

    findById(id) {
        for (let i = 0; i < this.categoryDao.length; i++) {
            if (id === this.categoryDao[i].id) {
                return this.categoryDao[i]
            }
        }
    }

    findByName(name){
        for (let i = 0; i < this.categoryDao.length; i++) {
            if (name === this.categoryDao[i].name) {
                return this.categoryDao[i]
            }
        }
    }
}


var object = new CategoryDao()

object.insertTable({id:1288,name: 'category1', categoryId: 1237})
object.insertTable({id:1298,name: 'category2',categoryId: 1233})
object.insertTable({id:1209, name:'category3',categoryId: 1235})
object.insertTable({id:1219, name:'category4',categoryId: 1231})

console.log(object.categoryDao);

console.log(object.updateTable({}));

console.log(object.deleteTable(2));

console.log(object.findAll());

console.log(object.findById(1288));

console.log(object.findByName('category4'));