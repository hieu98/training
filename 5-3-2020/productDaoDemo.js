class ProductDaoDemo {
    constructor() {
            this.productDaoDemo = []
    }
    insertTable(name, row) {
        if (name === 'product') {
            this.productDaoDemo.push(row)
            return this.productDaoDemo
        }
      
    }
    selectTable(name, where) {
        if (name === 'product') {
            for (let i = 0; i < this.productDaoDemo.length; i++) {
                if (this.productDaoDemo[i].id === where) {
                    return this.productDaoDemo[i]
                }
            }
        }
        
    }
    updateTable(name, row) {
        if (name === 'product') {
            this. productDaoDemo.splice(1, 1, row)
            return this.productDaoDemo
        }
        
    }
    deleteTable(name, row) {
        if (name === 'product') {
            this.productDaoDemo.splice(row, 1)
            return this. productDaoDemo
        }
        
       
    }
    truncateTable(name) {
        if (name === 'product') {
            this. productDaoDemo.splice(3)
            return this. productDaoDemo
        }
       
    }
}

var object = new ProductDaoDemo()

object.insertTable('product', {id:1238,name: 'product1', categoryId: 1237})
object.insertTable('product', {id:1248,name: 'product2', categoryId: 1233})
object.insertTable('product', {id:1258, name:'product3', categoryId: 1235})
object.insertTable('product', {id:1268, name:'product4', categoryId: 1231})

console.log(object.productDaoDemo);

console.log(object.selectTable('product', 1258));

console.log(object.updateTable('product', {}));

console.log(object.deleteTable('product', 3));

console.log(object.truncateTable('product'));
