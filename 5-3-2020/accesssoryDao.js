class AccesssoryDao {
    constructor() {
        this.accesssoryDao = []
    }
    insertTable(row) {
        this.accesssoryDao.push(row)
        return this.accesssoryDao
    }

    updateTable(row) {
        this.accesssoryDao.splice(1, 1, row)
        return this.accesssoryDao
    }

    deleteTable(row) {
        this.accesssoryDao.splice(row, 1)
        return this.accesssoryDao
    }

    findAll() {
        return this.accesssoryDao
    }

    findById(id) {
        for (let i = 0; i < this.accesssoryDao.length; i++) {
            if (id === this.accesssoryDao[i].id) {
                return this.accesssoryDao[i]
            }
        }
    }
}


var object = new AccesssoryDao()
object.insertTable({ id: 1228, name: 'accesssory1', categoryId: 1237 })
object.insertTable({ id: 1278, name: 'accesssory2', categoryId: 1233 })
object.insertTable({ id: 1208, name: 'accesssory3', categoryId: 1235 })
object.insertTable({ id: 1218, name: 'accesssory4', categoryId: 1231 })

console.log(object.accesssoryDao);

console.log(object.updateTable({}));

console.log(object.deleteTable(2));

console.log(object.findAll());

console.log(object.findById(1218));