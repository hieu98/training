class ProductDao {
    constructor() {
        this.productDao = []
    }
    insertTable(row) {
        this.productDao.push(row)
        return this.productDao
    }

    updateTable(row) {
        this.productDao.splice(1, 1, row)
        return this.productDao
    }

    deleteTable(row) {
        this.productDao.splice(row, 1)
        return this.productDao
    }

    findAll() {
        return this.productDao
    }

    findById(id) {
        for (let i = 0; i < this.productDao.length; i++) {
            if (id === this.productDao[i].id) {
                return this.productDao[i]
            }
        }
    }

    findByName(name) {
        for (let i = 0; i < this.productDao.length; i++) {
            if (name === this.productDao[i].name) {
                return this.productDao[i]
            }
        }
    }
}


var object = new ProductDao()

object.insertTable({ id: 1238, name: 'product1', categoryId: 1237 })
object.insertTable({ id: 1248, name: 'product2', categoryId: 1233 })
object.insertTable({ id: 1258, name: 'product3', categoryId: 1235 })
object.insertTable({ id: 1268, name: 'product4', categoryId: 1231 })

console.log(object.productDao);

console.log(object.updateTable({}));

console.log(object.deleteTable(2));

console.log(object.findAll());

console.log(object.findById(1238));

console.log(object.findByName('product4'));