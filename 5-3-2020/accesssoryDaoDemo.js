class AccesssoryDaoDemo {
    constructor() {
            this.accesssoryDaoDemo = []
    }
    insertTable(name, row) {
        if (name === 'accesssory') {
            this.accesssoryDaoDemo.push(row)
            return this.accesssoryDaoDemo
        }
      
    }
    selectTable(name, where) {
        if (name === 'accesssory') {
            for (let i = 0; i < this.accesssoryDaoDemo.length; i++) {
                if (this.accesssoryDaoDemo[i].id === where) {
                    return this.accesssoryDaoDemo[i]
                }
            }
        }
        
    }
    updateTable(name, row) {
        if (name === 'accesssory') {
            this. accesssoryDaoDemo.splice(1, 1, row)
            return this.accesssoryDaoDemo
        }
        
    }
    deleteTable(name, row) {
        if (name === 'accesssory') {
            this.accesssoryDaoDemo.splice(row, 1)
            return this. accesssoryDaoDemo
        }
        
       
    }
    truncateTable(name) {
        if (name === 'accesssory') {
            this. accesssoryDaoDemo.splice(3)
            return this. accesssoryDaoDemo
        }
       
    }
}

var object = new AccesssoryDaoDemo()

object.insertTable('accesssory', {id:1228,name: 'accesssory1', categoryId: 1237})
object.insertTable('accesssory', {id:1278,name: 'accesssory2',categoryId: 1233})
object.insertTable('accesssory', {id:1208, name:'accesssory3',categoryId: 1235})
object.insertTable('accesssory', {id:1218, name:'accesssory4',categoryId: 1231})

console.log(object.accesssoryDaoDemo);

console.log(object.selectTable('accesssory', 1228));

console.log(object.updateTable('accesssory', {}));

console.log(object.deleteTable('accesssory', 3));

console.log(object.truncateTable('accesssory'));
