class CategoryDaoDemo {
    constructor() {
            this.categoryDaoDemo = []
    }
    insertTable(name, row) {
        if (name === 'category') {
            this.categoryDaoDemo.push(row)
            return this.categoryDaoDemo
        }
      
    }
    selectTable(name, where) {
        if (name === 'category') {
            for (let i = 0; i < this.categoryDaoDemo.length; i++) {
                if (this.categoryDaoDemo[i].id === where) {
                    return this.categoryDaoDemo[i]
                }
            }
        }
        
    }
    updateTable(name, row) {
        if (name === 'category') {
            this. categoryDaoDemo.splice(1, 1, row)
            return this.categoryDaoDemo
        }
        
    }
    deleteTable(name, row) {
        if (name === 'category') {
            this.categoryDaoDemo.splice(row, 1)
            return this. categoryDaoDemo
        }
        
       
    }
    truncateTable(name) {
        if (name === 'category') {
            this. categoryDaoDemo.splice(3)
            return this. categoryDaoDemo
        }
       
    }
}

var object = new CategoryDaoDemo()

object.insertTable('category', {id:1288,name: 'category1', categoryId: 1237})
object.insertTable('category', {id:1298,name: 'category2',categoryId: 1233})
object.insertTable('category', {id:1209, name:'category3',categoryId: 1235})
object.insertTable('category', {id:1219, name:'category4',categoryId: 1231})

console.log(object.categoryDaoDemo);

console.log(object.selectTable('category', 1288));

console.log(object.updateTable('category', {}));

console.log(object.deleteTable('category', 3));

console.log(object.truncateTable('category'));
